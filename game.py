from random import randint #random is a library, randint is a function to get numbers in between two values

                                        #pseudocode
name = input("Hi! WHat is your name ")  #player_name = prompt the player for their name

for guess_number in range(1,6):     #guess_number = 1 to 6
    guess_year = randint(1924,2004) #month_number = generate a random number
    guess_month = randint(1,12)     #year_number = generate a random number

    print("Guess", guess_number,": ", name, "were you born in ",        #print this message
         guess_month, "/", guess_year)

    response = input("yes or no?")  #response = prompt the player with "yes or no"
    if response == "yes":
        print("I knew it")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
